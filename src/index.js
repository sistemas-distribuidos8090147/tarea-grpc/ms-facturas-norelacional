import app from './app.js';
import config from './config.js';
import './database.js';
import './serviceGrpc.js';

var port = config.PORT;

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
