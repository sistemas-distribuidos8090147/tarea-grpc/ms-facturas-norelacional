import grpc from '@grpc/grpc-js';
import protoLoader from '@grpc/proto-loader';
import config from './config.js';

const PROTO_FILE = './proto/service.proto';

const packageDefinition = protoLoader.loadSync(PROTO_FILE, {
    keepCase: true,
    longs: String,
    enums: String,
    arrays: true,
});

const facturaProto = grpc.loadPackageDefinition(packageDefinition);

const client = new facturaProto.FacturasService(
    `${config.GRPC_HOST}:${config.GRPC_PORT}`,
    grpc.credentials.createInsecure()
);

export default client;