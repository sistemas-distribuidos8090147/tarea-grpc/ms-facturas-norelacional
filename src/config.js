const config = {
    PORT: 3001,
    MONGO_URI: 'mongodb://localhost:27017/ms-facturas-norelacional',
    GRPC_HOST: 'localhost',
    GRPC_PORT: 3043
}

export default config;