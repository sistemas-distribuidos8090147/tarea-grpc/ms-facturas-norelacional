import { Router } from 'express';
import { createFactura, getFacturas } from '../controllers/factura.controller.js';

const router = Router()

router.post('/createFactura', createFactura);
router.get('/getFacturas', getFacturas);

export default router